
import static org.assertj.core.api.Assertions.assertThat;


import Repositories.CaregiverRepository;
import Repositories.DoctorRepository;
import com.example.demo.Caregiver;
import com.example.demo.Doctor;
import com.example.demo.User;
import Repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class CaregiverRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CaregiverRepository repo;

    // test methods go below

    @Test
    public void testCreateCaregiver() {
        Caregiver caregiver = new Caregiver();
        caregiver.setEmail("ravikumar@gmail.com");
        caregiver.setPassword("ravi2020");
        caregiver.setFirstName("Ravi");
        caregiver.setLastName("Kumar");

        Caregiver savedCaregiver = repo.save(caregiver);

        Caregiver existCaregiver = entityManager.find(Caregiver.class, savedCaregiver.getId());

        assertThat(caregiver.getEmail()).isEqualTo(existCaregiver.getEmail());

    }
}