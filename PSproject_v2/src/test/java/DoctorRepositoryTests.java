
import static org.assertj.core.api.Assertions.assertThat;


import Repositories.DoctorRepository;
import com.example.demo.Doctor;
import com.example.demo.User;
import Repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class DoctorRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DoctorRepository repo;

    // test methods go below

    @Test
    public void testCreateDoctor() {
        Doctor doctor = new Doctor();
        doctor.setEmail("ravikumar@gmail.com");
        doctor.setPassword("ravi2020");
        doctor.setFirstName("Ravi");
        doctor.setLastName("Kumar");

        Doctor savedDoctor = repo.save(doctor);

        Doctor existDoctor = entityManager.find(Doctor.class, savedDoctor.getId());

        assertThat(doctor.getEmail()).isEqualTo(existDoctor.getEmail());

    }
}