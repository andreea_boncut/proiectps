package Exceptions;

class CaregiverNotFoundException extends RuntimeException {

    CaregiverNotFoundException(Long id) {
        super("Could not find user " + id);
    }
}