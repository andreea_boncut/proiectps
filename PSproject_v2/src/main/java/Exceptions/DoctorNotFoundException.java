package Exceptions;

class DoctorNotFoundException extends RuntimeException {

    DoctorNotFoundException(Long id) {
        super("Could not find user " + id);
    }
}