package Controllers;

import java.util.List;

import com.example.demo.Doctor;
import Repositories.DoctorRepository;
import org.springframework.web.bind.annotation.*;

@RestController
//@RequestMapping("/Doctors")
/**
 * Clasa DoctorController contine metode pentru a realiza operatii de Get, Post, Delete
 */
class DoctorController {

    private final DoctorRepository repository;

    DoctorController(DoctorRepository repository) {
        this.repository = repository;
    }

    /**
     * metoda getAll afiseaza toate datele din tabela Doctors
     * @return
     */
    // Aggregate root
    // tag::get-aggregate-root[]
    @GetMapping("/Doctors")
    List<Doctor> all() {
        return repository.findAll();
    }
    // end::get-aggregate-root[]

    /**
     * metoda NewDoctor face post la un obiect de tip Doctor in tabela
     * @param newDoctor
     * @return
     */
    @PostMapping("/Doctors")
    Doctor NewDoctor(@RequestBody Doctor newDoctor) {
        return repository.save(newDoctor);
    }

    // Single item

    /**
     * metoda one returneaza obiectul cu id-ul cerut din tabela
     * @param id
     * @return
     */
    @GetMapping("/Doctors/{id}")
    Doctor one(@PathVariable Long id) {

        return repository.findById(id).get();

    }


    /**
     * metoda Delete sterge doctorul cu id-ul dat ca parametru
     *
     *
     *
     * * @param id
     */
    @DeleteMapping("/Doctors/{id}")
    void deleteDoctor(@PathVariable Long id) {
        repository.deleteById(id);
    }
}