package Controllers;

import java.util.List;

import Repositories.CaregiverRepository;
import com.example.demo.Caregiver;
import com.example.demo.Doctor;
import Repositories.DoctorRepository;
import org.springframework.web.bind.annotation.*;

@RestController
//@RequestMapping("/Doctors")
/**
 * Clasa CaregiverController contine metode pentru a realiza operatii de Get, Post, Delete
 */
class CaregiverController {

    private final CaregiverRepository repository;

    CaregiverController(CaregiverRepository repository) {
        this.repository = repository;
    }


    // Aggregate root
    // tag::get-aggregate-root[]
    /**
     * metoda getAll afiseaza toate datele din tabela Caregiver
     * @return
     */
    @GetMapping("/Caregivers")
    List<Caregiver> all() {
        return repository.findAll();
    }
    // end::get-aggregate-root[]

    /**
     * metoda NewCaregiver face post la un obiect de tip Caregiver in tabela
     * @param newCaregiver
     * @return
     */
    @PostMapping("/Caregivers")
    Caregiver NewCaregiver(@RequestBody Caregiver newCaregiver) {
        return repository.save(newCaregiver);
    }

    // Single item

    /**
     * metoda one returneaza obiectul cu id-ul cerut din tabela
     * @param id
     * @return
     */
    @GetMapping("/Caregiver/{id}")
    Caregiver one(@PathVariable Long id) {

        return repository.findById(id).get();

    }


    /**
     * metoda Delete sterge asistentul cu id-ul dat ca parametru
     *
     *
     *
     * * @param id
     */
    @DeleteMapping("/Doctors/{id}")
    void deleteCaregiver(@PathVariable Long id) {
        repository.deleteById(id);
    }
}