package Controllers;

import java.util.List;

import com.example.demo.User;
import Repositories.UserRepository;
import org.springframework.web.bind.annotation.*;

@RestController
//@RequestMapping("/Users")
/**
 * Clasa DoctorController contine metode pentru a realiza operatii de Get, Post, Delete
 */
class UserController {

    private final UserRepository repository;

    UserController(UserRepository repository) {
        this.repository = repository;
    }


    // Aggregate root
    // tag::get-aggregate-root[]
    /**
     * metoda getAll afiseaza toate datele din tabela Users
     * @return
     */
    @GetMapping("/Users")
    List<User> all() {
        return repository.findAll();
    }
    // end::get-aggregate-root[]

    /**
     * metoda newUser face post la un obiect de tip User in tabela
     * @param newUser
     * @return
     */
    @PostMapping("/Users")
    User newUser(@RequestBody User newUser) {
        return repository.save(newUser);
    }

    // Single item

    /**
     * metoda one returneaza obiectul cu id-ul cerut din tabela
     * @param id
     * @return
     */
    @GetMapping("/Users/{id}")
    User one(@PathVariable Long id) {

        return repository.findById(id).get();

    }


    /**
     * metoda Delete sterge userul cu id-ul dat ca parametru
     *
     *
     *
     * * @param id
     */
    @DeleteMapping("/Users/{id}")
    void deleteUser(@PathVariable Long id) {
        repository.deleteById(id);
    }
}