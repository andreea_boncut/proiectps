package Repositories;

import com.example.demo.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * interfata UserRepository extinde JpaRepository
 */
public interface UserRepository extends JpaRepository<User, Long> {

}