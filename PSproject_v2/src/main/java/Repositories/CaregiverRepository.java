package Repositories;

import com.example.demo.Caregiver;
import com.example.demo.Doctor;
import com.example.demo.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * interfata CaregiverRepository extinde JpaRepository
 */
public interface CaregiverRepository extends JpaRepository<Caregiver, Long> {

}