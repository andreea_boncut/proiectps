package Repositories;

import com.example.demo.Doctor;
import com.example.demo.User;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * interfata DoctorRepository extinde JpaRepository
 */
public interface DoctorRepository extends JpaRepository<Doctor, Long> {

}