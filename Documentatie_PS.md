# Clinica medicala
##### Boncut Andreea
##### Grupa 30239

Aplicatia reprezinta o clinica medicala online. Utilizatorii in cadrul acestei aplicatii sunt de 3 tipuri:
-	Doctor: are capacitatea de a manipula datele atat a asistentilor cat si a pacientilor si a medicamentelor
-	Pacient : drepturile acestuia sunt limitate la : vizualizarea planului medical si administrarea propriului cont 
-	Asistent: are dreptul de a vizualiza pacientii asociati si planul medical care le corespunde


### Functionalitati
##### 1. Creare plan medical: 


      Doctorul va putea crea un plan medical pentru fiecare pacient in care va trece lista medicamentelor de care are nevoie, intervalul la care se vor administra zilnic si perioada tratamentului.
##### 2. Gestionare pacienti: 


    Doctorul va putea adauga/sterge datele unui pacient din baza de date a clinicii. 


##### 3. Vizualizare/modificare date cont

    Fiecare pacient isi va adauga cateva informatii personale in cont. Acesta isi va introduce numele, prenumele, data nasterii, gen, adresa, si istoricul medical. 


##### 4. Vizualizare/modificare date cont

    Asistentul va putea vizualiza lista pacientilor pe care ii are in ingrijire, iar pentru fiecare dintre ei, va putea vizualiza planul de tratament.
    
## Tema 1
##### Pentru tema 1 am realizat:
- integrare Spring Boot
- conexiune la Baza de Date
- calluri Get, Post, Delete
- cateva Unit Tests
- generare JavaDoc
- documentatie MD


